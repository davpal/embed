#ifndef DIRECT_BUS_H_
#define DIRECT_BUS_H_

#include "systemc.h"

class direct_bus : public sc_interface, public sc_module {
private:
    int * data;
public:
    sc_in<bool> clk;
    direct_bus(sc_module_name name);
    ~direct_bus();
    virtual bool direct_read(int * data, unsigned int address);
    virtual bool direct_write(int * data, unsigned int address);
};

#endif