#include "systemc.h"
#include <ctime>
#include <sstream>
#include <string>
#include <iomanip>
#include "direct_bus.h"

unsigned int bit_count(unsigned int value) {
	unsigned int count = 0;
	while (value > 0) { // until all bits are zero
		if ((value & 1) == 1) // check lower bit
			count++;
		value >>= 1; // shift bits, removing lower bit
	}
	return count;
}

SC_MODULE (board) {
  sc_in  <bool     >  clk;
  sc_in  <sc_lv<6> >  switches;
  sc_out <sc_lv<6> >  red_led;
  sc_out <sc_lv<6> >  green_led;
  sc_out <sc_logic >  alarm_led;
  sc_port<direct_bus> bus;

  void light() {
    if(bit_count(switches.read().to_uint()) > 1) alarm_on();
    else alarm_off();
    green_led.write(switches.read());
    red_led.write(~switches.read());

    int green = green_led.read().to_int();
    int red =   red_led.read().to_int();
    int alarm = alarm_led.read().to_bool();

    bus->direct_write(&green, 0);
    bus->direct_write(&red,   1);
    bus->direct_write(&alarm,  2);
  }

  void alarm_on() {
    alarm_led = sc_logic(true);
  }

  void alarm_off() {
    alarm_led = sc_logic(false);
  }

  SC_CTOR(board) {
    SC_METHOD(light);
    sensitive << clk.pos();
  }
};

SC_MODULE(system_status) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;
  int clk_ticks;

  void status() {
    system("clear");
    int green, red, alarm;
    bus->direct_read(&green, 0);
    bus->direct_read(&red,   1);
    bus->direct_read(&alarm, 2);

    sc_lv<6> green_bits(green);
    sc_lv<6> red_bits(red);

    cout << "TICK NO. " << clk_ticks << '\n';
    std::cout << '[' << (alarm ? 'A' : ' ') << "]\n";
    for(int i = 5; i >= 0; i--) {
      std::cout << '[' << (green_bits[i].to_bool() ? 'G' : ' ') << "] ";
    }
    cout << " " << green_bits << '\n';
    for(int i = 5; i >= 0; i--) {
      std::cout << '[' << (red_bits[i].to_bool() ? 'R' : ' ') << "] ";
    }
    cout << " " << red_bits << '\n';
    ++clk_ticks;
  }

  SC_CTOR(system_status) {
    clk_ticks = 0;
    SC_METHOD(status);
    sensitive << clk;
  }
};

SC_MODULE (date_module) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;

  SC_CTOR (date_module) {
    SC_METHOD(get_date);
    sensitive << clk;
  }

  void get_date() {
    int data;
    bus->direct_read(&data, 0);
    sc_lv<6> green_bits(data);
    if(!green_bits[0].to_bool()) return;

    time_t t = time(0);


    bus->direct_write((int*)&t, 3);
  }
};

SC_MODULE (id_module) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;

  SC_CTOR (id_module) {
    SC_METHOD(set_car_id);
    sensitive << clk.neg();
  }

  void set_car_id() {
    int data, alarm, prev_id;
    bus->direct_read(&data, 0);
    bus->direct_read(&alarm, 2);
    bus->direct_read(&prev_id, 4);

    sc_lv<6> green_bits(data);

    if(prev_id != 0) return;
    if(!green_bits[1].to_bool() || alarm) return;

    std::cout << "Car id: ";
    int id;
    std::cin >> id;

    bus->direct_write(&id, 4);
  }
};

SC_MODULE (park_end) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;

  void set_end_date() {
    int data, alarm;
    bus->direct_read(&data, 0);
    bus->direct_read(&alarm, 2);
    sc_lv<6> green_bits(data);

    if(!green_bits[4].to_bool() || alarm) return;
    time_t end = time(0);

    bus->direct_write((int*)&end, 5);
  }

  SC_CTOR(park_end) {
    SC_METHOD(set_end_date);
    sensitive << clk;
  }
};

SC_MODULE (cost_module) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;

  SC_CTOR (cost_module) {
    SC_METHOD(calc_cost);
    sensitive << clk;
  }

  void calc_cost() {
    int data, alarm, date, end_time;
    bus->direct_read(&data, 0);
    bus->direct_read(&alarm, 2);
    sc_lv<6> green_bits(data);
    if(!green_bits[5].to_bool() || alarm) return;

    bus->direct_read(&date, 3);
    bus->direct_read(&end_time,  5);

    int hour_diff = (end_time - date) / 3600;

    int cost;
    if(hour_diff <= 1) cost = 350;
    if(hour_diff > 1 && hour_diff <= 2) cost = 650;
    if(hour_diff > 2) cost = hour_diff * 300;

    bus->direct_write(&cost, 6);
  }
};

SC_MODULE (print_module) {
  sc_in<bool> clk;
  sc_port<direct_bus> bus;

  SC_CTOR (print_module) {
    SC_METHOD(print_receipt);
    sensitive << clk;
  }

  std::string date_format(time_t t) {
    tm * now = localtime((time_t*)&t);
    std::stringstream ss;

    ss << (now->tm_year + 1900) << '-'
       << std::setfill('0') << (now->tm_mon  + 1) << '-'
       << std::setw(2) << now->tm_mday << ' '
       << std::setw(2) << now->tm_hour << ':'
       << std::setw(2) << now->tm_min  << ':'
       << std::setw(2) << now->tm_sec;

    return ss.str();
  }

  void print_receipt() {
    int data, alarm, date, end_time;
    bus->direct_read(&data, 0);
    bus->direct_read(&alarm, 2);
    sc_lv<6> green_bits(data);
    if(!green_bits[5].to_bool() || alarm) return;

    bus->direct_read(&date, 3);
    bus->direct_read(&end_time,  5);

    std::string start = date_format((time_t)date);
    std::string end = date_format((time_t)end_time);

    int id;
    bus->direct_read(&id, 4);

    int cost_int;
    bus->direct_read(&cost_int, 6);
    double cost = double(cost_int) / 100.0;

    std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::\n";
    std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::\n";
    std::cout << "::                                             ::\n";
    std::cout << "::                PARKOMAT PK                  ::\n";
    std::cout << ":: ------------------------------------------- ::\n";
    std::cout << ":: Nr rejestracyjny: " << std::setw(18) << id   << "        ::\n";
    std::cout << ":: Data parkowania : " << start << "        ::\n";
    std::cout << ":: Zakonczenie     : " << end   << "        ::\n";
    std::cout << ":: Naleznosc       : " << std::setw(18) << cost << "        ::\n";
    std::cout << "::                                             ::\n";
    std::cout << "::                                             ::\n";
    std::cout << "::                                             ::\n";
    std::cout << "::                DZIEKUJEMY!                  ::\n";
    std::cout << "::                                             ::\n";
    std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::\n";
    std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::\n";

    id = 0;
    bus->direct_write(&id, 4);
  }
};

int sc_main(int argc, char* argv[]) {
  sc_clock clk("clk", 2, SC_MS);
  sc_signal< sc_lv<6> > switches;
  sc_signal< sc_lv<6> > red_led;
  sc_signal< sc_lv<6> > green_led;
  sc_signal< sc_logic > alarm_led;
  direct_bus main_bus("bus");
  main_bus.clk(clk);

  board board("system");
  board.clk(clk);
  board.switches(switches);
  board.red_led(red_led);
  board.green_led(green_led);
  board.alarm_led(alarm_led);
  board.bus(main_bus);

  system_status system_status("system_status");
  system_status.clk(clk);
  system_status.bus(main_bus);

  date_module date_module("date");
  date_module.clk(clk);
  date_module.bus(main_bus);

  id_module id_module("id");
  id_module.clk(clk);
  id_module.bus(main_bus);

  park_end park_end("park_end");
  park_end.clk(clk);
  park_end.bus(main_bus);

  cost_module cost_module("cost");
  cost_module.clk(clk);
  cost_module.bus(main_bus);

  print_module print_module("print");
  print_module.clk(clk);
  print_module.bus(main_bus);

  switches = "000000";
  sc_start(2, SC_MS);

  int function;
  while(cin >> function) {
    if(function < 1 || function > 6) continue;

    sc_lv<6> state = switches.read();
    state[function - 1] = ~state[function - 1];

    switches.write(state);

    sc_start(4, SC_MS);
  }

  return 0;
}
