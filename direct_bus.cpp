#include "direct_bus.h"

direct_bus::direct_bus(sc_module_name name) {
  data = new int[10];
  memset(data, 0, sizeof(int)*10);
}

direct_bus::~direct_bus() {
  delete [] data;
}

bool direct_bus::direct_read(int * data, unsigned int address) {
  *data = this->data[address];
}

bool direct_bus::direct_write(int * data, unsigned int address) {
  this->data[address] = *data;
}
